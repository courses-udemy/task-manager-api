class CreateAccountUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :account_users, id: :uuid do |t|
      t.boolean :manager, default: false
      t.string :username, unique: true
      t.string :authentication_token, unique: true

      t.timestamps
    end
  end
end
