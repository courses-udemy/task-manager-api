class CreateAttachmentArchives < ActiveRecord::Migration[5.0]
  def change
    create_table :attachment_archives, id: :uuid do |t|
      t.string :type
      t.belongs_to :archivable, type: :uuid, polymorphic: true, index: true
      t.attachment :file

      t.timestamps
    end
  end
end
