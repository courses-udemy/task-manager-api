class CreateTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :tasks, id: :uuid do |t|
      t.belongs_to :user, type: :uuid, foreign_key: { to_table: :account_users }

      t.string :title
      t.text :description
      t.datetime :due_date, null: true, default: nil
      t.datetime :completed_at, null: true, default: nil

      t.timestamps
    end
  end
end
