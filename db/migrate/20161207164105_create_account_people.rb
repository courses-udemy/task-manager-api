class CreateAccountPeople < ActiveRecord::Migration[5.0]
  def change
    create_table :account_people, id: :uuid do |t|
      t.belongs_to :personable, type: :uuid, polymorphic: true, index: true
      t.string :nickname
      t.string :full_name
      t.string :document
      t.boolean :document_required, default: true
      t.string :gender
      t.string :nationality
      t.string :civil_status
      t.string :profession
      t.date :born_at

      t.timestamps
    end
  end
end
