# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170501155339) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"
  enable_extension "unaccent"

  create_table "account_people", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string   "personable_type"
    t.uuid     "personable_id"
    t.string   "nickname"
    t.string   "full_name"
    t.string   "document"
    t.boolean  "document_required", default: true
    t.string   "gender"
    t.string   "nationality"
    t.string   "civil_status"
    t.string   "profession"
    t.date     "born_at"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["personable_type", "personable_id"], name: "index_account_people_on_personable_type_and_personable_id", using: :btree
  end

  create_table "account_users", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.boolean  "manager",                default: false
    t.string   "username"
    t.string   "authentication_token"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.index ["email"], name: "index_account_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_account_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "attachment_archives", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string   "type"
    t.string   "archivable_type"
    t.uuid     "archivable_id"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["archivable_type", "archivable_id"], name: "index_attachment_archives_on_archivable_type_and_archivable_id", using: :btree
  end

  create_table "location_addresses", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string   "addressable_type"
    t.uuid     "addressable_id"
    t.uuid     "postal_code_id"
    t.string   "building_number"
    t.string   "complement"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["addressable_type", "addressable_id"], name: "index_location_addresses_on_addressable_type_and_addressable_id", using: :btree
    t.index ["postal_code_id"], name: "index_location_addresses_on_postal_code_id", using: :btree
  end

  create_table "location_cities", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid     "state_id"
    t.string   "name"
    t.integer  "ibge"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["state_id"], name: "index_location_cities_on_state_id", using: :btree
  end

  create_table "location_countries", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "location_postal_codes", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid     "city_id"
    t.string   "zip_code"
    t.string   "street_name"
    t.string   "neighborhood"
    t.decimal  "latitude"
    t.decimal  "longitude"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["city_id"], name: "index_location_postal_codes_on_city_id", using: :btree
  end

  create_table "location_regions", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid     "country_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["country_id"], name: "index_location_regions_on_country_id", using: :btree
  end

  create_table "location_states", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid     "region_id"
    t.string   "name"
    t.string   "abbreviation"
    t.integer  "ibge"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["region_id"], name: "index_location_states_on_region_id", using: :btree
  end

  create_table "tasks", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.uuid     "user_id"
    t.string   "title"
    t.text     "description"
    t.datetime "completed_at"
    t.datetime "due_date"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["user_id"], name: "index_tasks_on_user_id", using: :btree
  end

  add_foreign_key "location_addresses", "location_postal_codes", column: "postal_code_id"
  add_foreign_key "location_cities", "location_states", column: "state_id"
  add_foreign_key "location_postal_codes", "location_cities", column: "city_id"
  add_foreign_key "location_regions", "location_countries", column: "country_id"
  add_foreign_key "location_states", "location_regions", column: "region_id"
  add_foreign_key "tasks", "account_users", column: "user_id"
end
