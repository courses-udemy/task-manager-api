if Rails.env.development?
  ActiveRecord::Base.transaction do
    user = Account::User.create!(username: 'developer',
                                 email: 'developer@codde.io',
                                 password: ENV.fetch('DEVELOPER_USER_PASSWORD'))
    person = Account::Person.new(personable: user, full_name: 'Developer')
    person.save!(validate: false)
  end
end
