# frozen_string_literal: true

require 'rails_helper'

describe BootstrapHelper do
  let(:message) { Faker::Lorem.sentence }

  describe '#bootstrap_alert' do
    let(:out) { "<div class=\"alert alert-info no-margin\">#{message}</div>" }

    it { expect(bootstrap_alert(:info, message)).to eq(raw(out)) }
  end

  describe '#bootstrap_alert_info' do
    let(:out) { "<div class=\"alert alert-info no-margin\">#{message}</div>" }

    it { expect(bootstrap_alert_info(message)).to eq(raw(out)) }
  end

  describe '#bootstrap_alert_warning' do
    let(:out) { "<div class=\"alert alert-warning no-margin\">#{message}</div>" }

    it { expect(bootstrap_alert_warning(message)).to eq(raw(out)) }
  end

  describe '#bootstrap_alert_success' do
    let(:out) { "<div class=\"alert alert-success no-margin\">#{message}</div>" }

    it { expect(bootstrap_alert_success(message)).to eq(raw(out)) }
  end

  describe '#bootstrap_alert_danger' do
    let(:out) { "<div class=\"alert alert-danger no-margin\">#{message}</div>" }

    it { expect(bootstrap_alert_danger(message)).to eq(raw(out)) }
  end

  describe '#bootstrap_alert_not_found' do
    include TranslationHelper

    let(:model) { Account::User }
    let(:out_male) do
      message = "Nenhum #{tm(model).downcase} foi encontrado"
      "<div class=\"alert alert-info no-margin\">#{message}</div>"
    end
    let(:out_female) do
      message = "Nenhuma #{tm(model).downcase} foi encontrada"
      "<div class=\"alert alert-info no-margin\">#{message}</div>"
    end

    context 'when gender is male' do
      it { expect(bootstrap_alert_not_found(:male, model)).to eq(raw(out_male)) }
    end

    context 'when gender is female' do
      it { expect(bootstrap_alert_not_found(:female, model)).to eq(raw(out_female)) }
    end

    context '#bootstrap_alert_not_found aliases' do
      describe '#bootstrap_alert_not_found_male' do
        it { expect(bootstrap_alert_not_found_male(model)).to eq(raw(out_male)) }
      end

      describe '#bootstrap_alert_not_found_female' do
        it { expect(bootstrap_alert_not_found_female(model)).to eq(raw(out_female)) }
      end
    end
  end
end
