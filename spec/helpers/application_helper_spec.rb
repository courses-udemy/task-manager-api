# frozen_string_literal: true

require 'rails_helper'

describe ApplicationHelper do
  let(:title) { 'Task Manager Api' }

  describe 'app_page_title' do
    context 'without subtitle' do
      it { expect(app_page_title).to eq("[DEVELOPMENT] #{title}") }
    end

    context 'with subtitle' do
      let(:subtitle) { Faker::Hipster.sentence }
      let(:page_title) { "[DEVELOPMENT] #{subtitle} - #{title}" }

      it { expect(app_page_title(subtitle)).to eq(page_title) }
    end
  end

  describe 'admin_page_title' do
    context 'without subtitle' do
      it { expect(admin_page_title).to eq("ADMIN :: [DEVELOPMENT] #{title}") }
    end

    context 'with subtitle' do
      let(:subtitle) { Faker::Hipster.sentence }
      let(:page_title) { "ADMIN :: [DEVELOPMENT] #{subtitle} - #{title}" }

      it { expect(admin_page_title(subtitle)).to eq(page_title) }
    end
  end
end
