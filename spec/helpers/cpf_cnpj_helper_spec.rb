# frozen_string_literal: true

require 'rails_helper'

describe CpfCnpjHelper do
  describe '#cpf_format' do
    let(:cpf) { Faker::CPF.numeric }
    let(:regex) { /[0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2}/ }

    it { expect(cpf_format(cpf)).to match(regex) }
  end

  describe '#cnpj_format' do
    let(:cnpj) { Faker::CNPJ.numeric }
    let(:regex) { %r([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2}) }

    it { expect(cnpj_format(cnpj)).to match(regex) }
  end
end
