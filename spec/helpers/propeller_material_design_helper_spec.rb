# frozen_string_literal: true

require 'rails_helper'

describe PropellerMaterialDesignHelper do
  describe 'pmd_icon' do
    it 'generate icon without options' do
      expect(pmd_icon('person')).to eq('<i class="material-icons">person</i>')
    end

    it 'generate icon with options' do
      out = '<i foo="bar" class="material-icons">person</i>'
      expect(pmd_icon('person', foo: 'bar')).to eq(out)
    end

    it 'generate icon with class options' do
      out = '<i class="material-icons foo-bar">person</i>'
      expect(pmd_icon('person', class: 'foo-bar')).to eq(out)
    end
  end

  describe 'pmd_icon_text' do
    it 'generate icon text without options' do
      out = '<i class="material-icons">person</i> my text'
      expect(pmd_icon_text('person', 'my text')).to eq(out)
    end

    it 'generate icon text with options' do
      out = '<i foo="bar" class="material-icons">person</i> my text'
      expect(pmd_icon_text('person', 'my text', foo: 'bar')).to eq(out)
    end

    it 'generate icon text with class options' do
      out = '<i class="material-icons foo-bar">person</i> my text'
      expect(pmd_icon_text('person', 'my text', class: 'foo-bar')).to eq(out)
    end
  end

  describe 'fa_classes' do
    it 'generate classes without options' do
      expect(pmd_classes(class: 'person')).to eq('material-icons person')
    end
  end
end
