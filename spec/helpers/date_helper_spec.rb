# frozen_string_literal: true

require 'rails_helper'

describe DateHelper do
  let(:today) { Date.current }
  let(:monday) { Date.current.beginning_of_week }
  let(:friday) { Date.current.beginning_of_week + 4.days }
  let(:week) { monday..friday }
  let(:dates_format) { '%d/%m/%Y' }
  let(:times_format) { '%H:%M' }
  let(:default_format) { "#{dates_format} #{times_format}" }

  describe '#current_week' do
    it { expect(current_week).to eq(week) }
  end

  describe '#current_monday' do
    it { expect(current_monday).to eq(monday) }
  end

  describe '#current_friday' do
    it { expect(current_friday).to eq(friday) }
  end

  describe '#locale_date' do
    context 'when date nil' do
      it { expect(locale_date(nil)).to be_nil }
    end

    context 'when date is valid' do
      it { expect(locale_date(today)).to eq(today.strftime(dates_format)) }
    end
  end

  describe '#date_format' do
    context 'when format nil' do
      it { expect(date_format(today)).to eq(today.strftime(default_format)) }
    end

    context 'when format only date' do
      it { expect(date_format(today, dates_format)).to eq(today.strftime(dates_format)) }
    end

    context 'when format only time' do
      it { expect(date_format(today, times_format)).to eq(today.strftime(times_format)) }
    end
  end
end
