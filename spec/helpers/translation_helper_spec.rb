# frozen_string_literal: true

require 'rails_helper'

describe TranslationHelper do
  let(:singular_user_model_name) { I18n.t('activerecord.models.account/user.one') }
  let(:plural_user_model_name) { I18n.t('activerecord.models.account/user.other') }
  let(:attribute_user_email) { I18n.t('activerecord.attributes.account/user.email') }

  let(:boolean_truly) { I18n.t('boolean.truly') }
  let(:boolean_falsely) { I18n.t('boolean.falsely') }

  describe '#translate_model_name' do
    it { expect(tm(Account::User)).to eq(singular_user_model_name) }
    it { expect(tm(Account::User, count: 2)).to eq(plural_user_model_name) }
  end

  describe '#translate_model_name_pluralized' do
    it { expect(tmp(Account::User)).to eq(plural_user_model_name) }
  end

  describe '#translate_model_attribute' do
    it { expect(ta(Account::User, :email)).to eq(attribute_user_email) }
  end

  describe '#translate_boolean' do
    context 'needs to evaluate like ruby expressions' do
      it { expect(tb(true)).to eq(boolean_truly) }
      it { expect(tb(5353)).to eq(boolean_truly) }

      it { expect(tb(false)).to eq(boolean_falsely) }
      it { expect(tb(nil)).to eq(boolean_falsely) }
    end
  end

  describe '#translate_boolean_in_icon' do
    include FontAwesomeHelper

    let(:truly_icon) { '<i class="fa fa-check text-success"></i>' }
    let(:falsely_icon) { '<i class="fa fa-times text-danger"></i>' }

    context 'needs to evaluate like ruby expressions' do
      it { expect(tbi(true)).to eq(truly_icon) }
      it { expect(tbi(5353)).to eq(truly_icon) }

      it { expect(tbi(false)).to eq(falsely_icon) }
      it { expect(tbi(nil)).to eq(falsely_icon) }
    end
  end
  context 'when use FontAwesomeHelper' do
    include FontAwesomeHelper

    describe '#translate_boolean_in_icon' do
      let(:truly_icon) { '<i class="fa fa-check text-success"></i>' }
      let(:falsely_icon) { '<i class="fa fa-times text-danger"></i>' }

      context 'needs to evaluate like ruby expressions' do
        it { expect(tbi(true)).to eq(truly_icon) }
        it { expect(tbi(5353)).to eq(truly_icon) }

        it { expect(tbi(false)).to eq(falsely_icon) }
        it { expect(tbi(nil)).to eq(falsely_icon) }
      end
    end

    describe '#translate_boolean_in_check_box_icon' do
      let(:truly_icon) { '<i class="fa fa-check-square-o text-success"></i>' }
      let(:falsely_icon) { '<i class="fa fa-square-o text-default"></i>' }

      context 'needs to evaluate like ruby expressions' do
        it { expect(tbci(true)).to eq(truly_icon) }
        it { expect(tbci(5353)).to eq(truly_icon) }

        it { expect(tbci(false)).to eq(falsely_icon) }
        it { expect(tbci(nil)).to eq(falsely_icon) }
      end
    end
  end
end
