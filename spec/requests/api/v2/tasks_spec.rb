# frozen_string_literal: true

require 'rails_helper'

describe Api::V2 do
  describe 'Tasks API' do
    before { host! 'api.localhost.dev' }

    let(:task) { create(:task, user_id: current_user.id) }
    let(:task_params) { attributes_for(:task) }
    let(:invalid_task_params) { attributes_for(:invalid_task) }

    describe 'GET /v2/tasks' do
      before { get '/v2/tasks', params: {}, headers: headers }

      context 'when user have no tasks' do
        it 'returns status code 200' do
          expect(response).to have_http_status(:success)
        end

        it 'returns the json data for the created task' do
          expect(json_body[:data]).to be_empty
        end
      end

      context 'when user have 5 tasks' do
        before do
          create_list(:task, 5, user_id: current_user.id)
          get '/v2/tasks', params: {}, headers: headers
        end

        it 'returns status code 200' do
          expect(response).to have_http_status(:success)
        end

        it 'returns the json data for the created task' do
          expect(json_body[:data].count).to eq(5)
        end
      end
    end

    describe 'GET /v2/tasks/:id' do
      context 'when task nonexistent' do
        before { get "/v2/tasks/#{invalid_uuid}", params: {}, headers: headers }

        it 'returns status code 404' do
          expect(response).to have_http_status(:not_found)
        end

        it 'returns the json data for the created task' do
          expect(json_body).to have_key(:errors)
        end
      end

      context 'when task existent' do
        before { get "/v2/tasks/#{task.id}", params: {}, headers: headers }

        it 'returns status code 200' do
          expect(response).to have_http_status(:success)
        end

        it 'returns the json data for the created task' do
          expect(json_body[:data][:attributes][:title]).to eq(task.title)
        end
      end
    end

    describe 'POST /v2/tasks' do
      context 'when the params are invalid' do
        before { post '/v2/tasks', params: { task: invalid_task_params }, headers: headers }

        it 'returns status code 422' do
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'saves the task in database' do
          expect(Task.find_by(title: task_params[:title])).to be_nil
        end

        it 'returns the json data for the errors' do
          expect(json_body).to have_key(:errors)
        end
      end

      context 'when the params are valid' do
        before { post '/v2/tasks', params: { task: task_params }, headers: headers }

        it 'returns status code 201' do
          expect(response).to have_http_status(:created)
        end

        it 'saves the task in database' do
          expect(Task.find_by(title: task_params[:title])).not_to be_nil
        end
      end
    end

    describe 'PUT /v2/tasks/:id' do
      context 'when task not found' do
        before { put "/v2/tasks/#{invalid_uuid}", params: {}, headers: headers }

        it 'returns status code 404' do
          expect(response).to have_http_status(:not_found)
        end

        it 'returns the json data for the errors' do
          expect(json_body).to have_key(:errors)
        end
      end

      context 'when the params are invalid' do
        before do
          put "/v2/tasks/#{task.id}", params: { task: invalid_task_params }, headers: headers
        end

        it 'returns status code 422' do
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'saves the task in database' do
          expect(Task.find_by(title: task_params[:title])).to be_nil
        end

        it 'returns the json data for the errors' do
          expect(json_body).to have_key(:errors)
        end
      end

      context 'when the params are valid' do
        before { put "/v2/tasks/#{task.id}", params: { task: task_params }, headers: headers }

        it 'returns status code 200' do
          expect(response).to have_http_status(:success)
        end

        it 'saves the task in database' do
          expect(Task.find_by(title: task_params[:title])).not_to be_nil
        end
      end
    end

    describe 'DELETE /v2/tasks/:id' do
      context 'when task not found' do
        before { delete "/v2/tasks/#{invalid_uuid}", params: {}, headers: headers }

        it 'returns status code 404' do
          expect(response).to have_http_status(:not_found)
        end

        it 'returns the json data for the errors' do
          expect(json_body).to have_key(:errors)
        end
      end

      context 'when task found' do
        before { delete "/v2/tasks/#{task.id}", params: {}, headers: headers }

        it 'returns status code 204' do
          expect(response).to have_http_status(:no_content)
        end

        it 'saves the task in database' do
          expect { Task.find(task.id) }.to raise_error(ActiveRecord::RecordNotFound)
        end
      end
    end
  end
end
