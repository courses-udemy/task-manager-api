# frozen_string_literal: true

require 'rails_helper'

describe Api::V2 do
  describe 'Home API' do
    before { host! 'api.localhost.dev' }

    describe 'GET /v2' do
      before { get '/v2' }

      it 'visit home' do
        expect(json_body[:app_name]).to eq(ENV.fetch('APP_PAGE_TITLE'))
      end

      it { expect(response).to have_http_status(:success) }
    end
  end
end
