# frozen_string_literal: true

require 'rails_helper'

describe Api::V1::Account do
  describe 'Sessions API' do
    before { host! 'api.localhost.dev' }

    let(:user) { create(:session_account_user) }

    describe 'POST /v1/account/sessions' do
      before { post '/v1/account/sessions', params: { session: session_params } }

      context 'when the request params are valid' do
        let(:session_params) { { email: user.email, password: '123456' } }

        it 'returns status code 200' do
          expect(response).to have_http_status(:success)
        end

        it 'returns the json data for the created user' do
          expect(json_body[:email]).to eq(user.email)
        end

        it 'returns authorization token for the created user' do
          expect(response.headers).to have_key('Authorization-Token')
        end

        it 'authorization token should have user_id' do
          decoded = JsonWebToken::Decode.call(response.headers['Authorization-Token'])
          expect(decoded[:user_id]).to eq(user.id)
        end
      end

      context 'when the request params are invalid' do
        let(:session_params) { attributes_for(:invalid_account_user) }

        it 'returns status code 401' do
          expect(response).to have_http_status(:unauthorized)
        end

        it 'returns the json data for the errors' do
          expect(json_body).to have_key(:errors)
        end
      end
    end

    describe 'DELETE /v1/account/sessions/:id' do
      before { delete "/v1/account/sessions/#{user.authentication_token}" }

      it 'returns status code 204' do
        expect(response).to have_http_status(:no_content)
      end

      it 'user was removed' do
        expect(::Account::User.find_by(authentication_token: user.authentication_token)).to be_nil
      end
    end
  end
end
