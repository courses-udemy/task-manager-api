# frozen_string_literal: true

require 'rails_helper'

describe Api::V1::Account do
  describe 'Registrations API' do
    before { host! 'api.localhost.dev' }

    describe 'POST /v1/account/registrations' do
      before { post '/v1/account/registrations', params: { registration: registration_params } }

      context 'when the request params are valid' do
        let(:registration_params) { attributes_for(:account_user) }

        it 'returns status code 201' do
          expect(response).to have_http_status(:created)
        end

        it 'returns the json data for the created user' do
          expect(json_body[:email]).to eq(registration_params[:email])
        end
      end

      context 'when the request params are invalid' do
        let(:registration_params) { attributes_for(:invalid_account_user) }

        it 'returns status code 422' do
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'returns the json data for the errors' do
          expect(json_body).to have_key(:errors)
        end
      end
    end
  end
end
