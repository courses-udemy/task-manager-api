# frozen_string_literal: true

require 'rails_helper'

describe Api::V1::Account do
  describe 'Users API' do
    before { host! 'api.localhost.dev' }

    let(:user_params) { attributes_for(:account_user) }
    let(:invalid_user_params) { attributes_for(:invalid_account_user) }

    describe 'GET /v1/account/users/:id' do
      context 'when user existent' do
        before { get "/v1/account/users/#{current_user.id}", params: {}, headers: headers }

        it 'returns status code 200' do
          expect(response).to have_http_status(:success)
        end

        it 'returns the json data for the created user' do
          expect(json_body[:email]).to eq(current_user.email)
        end
      end
    end

    describe 'PUT /v1/account/users/:id' do
      context 'when user existent' do
        before do
          put "/v1/account/users/#{current_user.id}", params: { user: invalid_user_params },
                                                      headers: headers
        end

        it 'returns status code 422' do
          expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'returns the json data for the created user' do
          expect(json_body).to have_key(:errors)
        end
      end

      context 'when user existent' do
        before do
          put "/v1/account/users/#{current_user.id}", params: { user: user_params },
                                                      headers: headers
        end

        it 'returns status code 200' do
          expect(response).to have_http_status(:success)
        end

        it 'returns the json data for the created user' do
          expect(json_body[:email]).to eq(user_params[:email])
        end
      end
    end
  end
end
