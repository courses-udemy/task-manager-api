# frozen_string_literal: true

require 'rails_helper'

describe Authenticable do
  controller(ApplicationController) do
    # rubocop:disable RSpec/DescribedClass
    include Authenticable
  end

  let(:app_controller) { subject }

  describe '#current_user' do
    let(:user) { create(:account_user) }
    let(:authorization_token) { JsonWebToken::Encode.call(user_id: user.id) }

    context 'when authorization token valid' do
      before do
        req = double(headers: { 'Authorization-Token' => authorization_token })
        allow(app_controller).to receive(:request).and_return(req)
      end

      it 'returns the user from the authorization header' do
        expect(app_controller.current_user).to eq(user)
      end
    end

    context 'when authorization token empty' do
      before do
        req = double(headers: { 'Authorization-Token' => '' })
        allow(app_controller).to receive(:request).and_return(req)
      end

      it 'returns the user from the authorization header' do
        expect(app_controller.current_user).to eq(nil)
      end
    end
  end

  describe '#http_authorization_header!' do
    controller do
      before_action :http_authorization_header!

      def authenticated_action; end
    end

    let(:invalid_authorization_token) { JsonWebToken::Encode.call(user_id: SecureRandom.uuid) }

    context 'when there is no user logged in' do
      before do
        headers = { 'Authorization-Token' => invalid_authorization_token }
        routes.draw { get 'authenticated_action' => 'anonymous#authenticated_action' }

        get :authenticated_action, params: {}, headers: headers
      end

      xit 'return status code 401' do
        expect(response).to have_http_status(:unauthorized)
      end

      it 'returns the json data for the errors' do
        expect(json_body).to have_key(:errors)
      end
    end

    context 'when authorization token empty' do
      before do
        headers = { 'Authorization-Token' => '' }
        routes.draw { get 'authenticated_action' => 'anonymous#authenticated_action' }

        get :authenticated_action, params: {}, headers: headers
      end

      it 'return status code 403' do
        expect(response).to have_http_status(:forbidden)
      end

      it 'returns the json data for the errors' do
        expect(json_body).to have_key(:errors)
      end
    end
  end
end
