# frozen_string_literal: true

require 'rails_helper'

module Admin
  module Account
    describe HomeController do
      login_user

      describe 'GET index' do
        before { get :index }

        it 'returns http success' do
          expect(response).to have_http_status(:success)
        end

        it 'renders the index template' do
          expect(response).to render_template(:index)
        end
      end
    end
  end
end
