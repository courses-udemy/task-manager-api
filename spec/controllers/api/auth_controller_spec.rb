# frozen_string_literal: true

require 'rails_helper'

describe Api::AuthController do
  describe 'includes the correct concerns' do
    it { expect(controller.class.ancestors).to include(Authenticable) }
  end
end
