# frozen_string_literal: true

require 'rails_helper'

describe HomeController do
  describe 'GET index' do
    before { get :index }

    it 'returns http success' do
      expect(response).to have_http_status(:success)
    end

    it 'renders the index template' do
      expect(response).to render_template(:index)
    end
  end
end
