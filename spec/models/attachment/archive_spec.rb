# frozen_string_literal: true

require 'rails_helper'

describe Attachment::Archive do
  context 'associations' do
    it { is_expected.to belong_to(:archivable) }
  end
end
