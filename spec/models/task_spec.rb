# frozen_string_literal: true

require 'rails_helper'

describe Task do
  context 'associations' do
    it { is_expected.to belong_to(:user) }
  end

  context 'validations' do
    it { is_expected.to validate_presence_of(:title) }
    it { is_expected.to validate_presence_of(:description) }
  end

  context 'factories' do
    it { expect(build(:task)).to be_valid }
    it { expect(build(:invalid_task)).not_to be_valid }
  end
end
