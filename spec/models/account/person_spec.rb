# frozen_string_literal: true

require 'rails_helper'

module Account
  describe Person do
    context 'associations' do
      it { is_expected.to belong_to(:personable) }

      it { is_expected.to have_one(:avatar) }
    end

    context 'nested attributes' do
      it { is_expected.to accept_nested_attributes_for(:avatar) }
    end

    context 'validations' do
      it { is_expected.to validate_presence_of(:full_name) }
      it { is_expected.to validate_presence_of(:gender) }
      it { is_expected.to validate_inclusion_of(:gender).in_array(Gender.list) }
      it { is_expected.to validate_presence_of(:civil_status) }
      it { is_expected.to validate_inclusion_of(:civil_status).in_array(CivilStatus.list) }
      it { is_expected.to validate_presence_of(:born_at) }
      it { is_expected.to validate_presence_of(:document) }
    end

    context 'factories' do
      it { expect(build(:account_person)).to be_valid }
      it { expect(build(:invalid_account_person)).not_to be_valid }
    end
  end
end
