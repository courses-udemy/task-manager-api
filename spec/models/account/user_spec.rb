# frozen_string_literal: true

require 'rails_helper'

module Account
  describe User do
    context 'associations' do
      it { is_expected.to have_one(:person) }

      it { is_expected.to have_many(:tasks).dependent(:destroy) }
    end

    context 'nested attributes' do
      it { is_expected.to accept_nested_attributes_for(:person) }
    end

    context 'delegates' do
      it { is_expected.to respond_to(:full_name) }
      it { is_expected.to respond_to(:document) }
    end

    context 'validations' do
      it { is_expected.to validate_presence_of(:email) }
      it { is_expected.to validate_uniqueness_of(:email).ignoring_case_sensitivity }
      it { is_expected.to validate_presence_of(:username) }
      it { is_expected.to validate_uniqueness_of(:username) }
      it { is_expected.to validate_presence_of(:password) }
    end

    context 'factories' do
      it { expect(build(:account_user)).to be_valid }
      it { expect(build(:invalid_account_user)).not_to be_valid }
    end

    describe '#generate_authentication_token!' do
      subject(:user) { described_class.new }

      it 'generate a unique auth token' do
        allow(Devise).to receive(:friendly_token).and_return('abc123xyzTOKEN')
        user.generate_authentication_token!

        expect(user.authentication_token).to eq('abc123xyzTOKEN')
      end

      it 'generate another token when the current authentication_token already has been taken' do
        allow(Devise).to receive(:friendly_token).and_return('Token123', 'Token123', 'abc123xyz')
        existing_user = create(:account_user, authentication_token: 'Token123')
        user.generate_authentication_token!

        expect(user.authentication_token).not_to eq(existing_user.authentication_token)
      end
    end
  end
end
