# frozen_string_literal: true

FactoryGirl.define do
  factory :invalid_attachment_archive, class: Attachment::Archive do
    type nil
    attachable nil
    file nil
  end
end
