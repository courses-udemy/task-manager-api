# frozen_string_literal: true

FactoryGirl.define do
  factory :account_person, class: Account::Person do
    full_name Faker::Name.name
    gender Gender.list.sample
    nationality Faker::Address.country
    civil_status CivilStatus.list.sample
    profession Faker::Superhero.name
    born_at 5.years.ago
    document Faker::CPF.numeric

    association :personable, factory: :account_user
  end

  factory :invalid_account_person, class: Account::Person do
    full_name nil
    gender nil
    nationality nil
    civil_status nil
    profession nil
    born_at nil
    personable nil
  end
end
