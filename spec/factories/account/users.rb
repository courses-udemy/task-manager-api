# frozen_string_literal: true

FactoryGirl.define do
  factory :account_user, class: Account::User do
    username { Faker::Internet.user_name }
    email { Faker::Internet.email }
    password Faker::Internet.password(8, 24)
    authentication_token nil
  end

  factory :invalid_account_user, class: Account::User do
    username nil
    email nil
    password nil
    authentication_token nil
  end

  factory :session_account_user, class: Account::User do
    username { Faker::Internet.user_name }
    email { Faker::Internet.email }
    password '123456'
    authentication_token nil
  end
end
