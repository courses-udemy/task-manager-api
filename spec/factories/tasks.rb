# frozen_string_literal: true

FactoryGirl.define do
  factory :task do
    title { Faker::Lorem.sentence }
    description { Faker::Lorem.sentence }
    due_date { Faker::Date.forward }
    completed_at nil

    user factory: :account_user
  end

  factory :invalid_task, class: Task do
    title nil
    description nil
    due_date nil
    completed_at nil

    user nil
  end
end
