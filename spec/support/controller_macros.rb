# frozen_string_literal: true

module ControllerMacros
  def login_user
    before do
      @request.env['devise.mapping'] = Devise.mappings[:user]

      @current_user ||= create(:session_account_user)

      sign_in current_user
    end
  end

  def current_user
    @current_user
  end

  def current_user_id
    @current_user_id ||= current_user.id
  end
end
