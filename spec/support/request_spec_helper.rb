# frozen_string_literal: true

module RequestSpecHelper
  def current_user
    @current_user ||= create(:account_user)
  end

  def invalid_uuid
    SecureRandom.uuid
  end

  def headers
    @headers ||= { 'Authorization-Token' => authorization_token }
  end

  def authorization_token
    @authorization_token ||= JsonWebToken::Encode.call(user_id: current_user.id)
  end

  def invalid_headers
    @invalid_headers ||= { 'Authorization-Token' => invalid_authorization_token }
  end

  def invalid_authorization_token
    @invalid_authorization_token = JsonWebToken::Encode.call(user_id: invalid_uuid)
  end

  def json_body
    @json_body ||= JSON.parse(response.body, symbolize_names: true)
  end
end
