# Project Base with Ruby on Rails 5.0.2 by [Codde.io](http://codde.io)

## Requirements
- Ruby 2.4.1 
- Rails 5.0.2 
- PostgreSQL 9.5.6
- Redis 3.0.6

## Download and change git repository

```bash
git clone git@bitbucket.org:coddeio/rails-base.git your-project-name
git remote add base git@bitbucket.org:coddeio/rails-base.git
git remote rm origin
git remote add origin git@bitbucket.org:coddeio/your-project-name.git
git push -u origin master
```

On file `config/application.rb:19` change the name app from `RailsBase` to `YourProjectName`

## Installation

#### Application

```bash
bundle install
cp .env.sample .env
```

Set your `APP_NAME=your_project_name` into environment file `.env`

The database names will be "your_project_name_development" and "your_project_name_test"

#### Database

```bash
rails db:drop db:create db:migrate db:seed
```

## Run application

```bash
bundle exec rails s
```
#### Rubocop

```bash
rubocop # to verify
# or
rubocop --auto-correct # to fix
```

#### Rspec

```bash
bundle exec spring rspec
```

#### Brakeman

```bash
brakeman
```
