Antes de darmos prosseguimento, vamos fazer alguns ajustes:
	- remover o devise_for :users (config/routes.rb)
			>> git commit -m "Disabling devise routes (temporarily)"
	- mudar aspas duplas para aspas simples (routes.rb, spec/requests/api/v1/users_specs.rb, factories/users.rb, spec/models/user_spec.rb)
			>> git commit -m "changing double quotes to simple quotes"