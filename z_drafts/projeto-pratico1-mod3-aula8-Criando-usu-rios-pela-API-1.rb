Vamos adicionar mais um endpoint em nossa API, dessa vez para a cria��o de usu�rios

Adicionando testes para "POST /users"

 describe "POST /users" do
    before do
      headers = { "Accept" => "application/vnd.taskmanager.v1" }
      post '/users', params: { user: user_params }, headers: headers
    end

    context 'when the request params are valid' do
      let(:user_params) { attributes_for(:user) }

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end

      it 'returns the json data for the created user' do
        user_response = JSON.parse(response.body)
        expect(user_response['email']).to eq(user_params[:email])
      end
    end

    context 'when the request params are invalid' do
      let(:user_params) { attributes_for(:user, email: 'invalid.email@') }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns the json data for the errors' do
        user_response = JSON.parse(response.body)
        expect(user_response).to have_key('errors')
      end
    end
  end
  

Testar e fazer as corre��es de acordo com o teste
	>> bundle exec spring rspec spec/requests/api/v1/users_spec.rb
	- adicionar rota
	- adicionar action no controller
	- corrigir '' para : (expect(user_response['email']).to eq(user_params[:email])
	

Comitar o c�digo:
	>> git add .
	>> git commit -m "Adding create action to the users controller with tests"



Adiciona o par�metro "symbolize_names: true" (no m�todo JSON.parse)
	- corrigir par�metros que utilizaram '' para utilizarem :
	
Comitar altera��o
	>> git add .
	>> git commit -m "Symbolizing response body keys"

	