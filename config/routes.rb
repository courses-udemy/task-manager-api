# frozen_string_literal: true

Rails.application.routes.draw do
  root 'home#index'

  devise_for :users, class_name: 'Account::User'

  namespace :admin do
    root 'home#index', as: :root

    namespace :account do
      root 'home#index', as: :root
      resources :users
    end
  end

  namespace :api, defaults: { format: :json }, constraints: { subdomain: 'api' }, path: '/' do
    api_version module: 'V1', path: { value: 'v1' } do
      root 'home#index', as: :root
      namespace :account do
        resources :sessions, only: [:create, :destroy]
        resources :registrations, only: [:create]
        resources :users, only: [:show, :update]
      end
      resources :tasks, only: [:index, :show, :create, :update, :destroy]
    end

    api_version module: 'V2', path: { value: 'v2' }, default: true do
      root 'home#index', as: :root
      namespace :account do
        resources :sessions, only: [:create, :destroy]
        resources :registrations, only: [:create]
        resources :users, only: [:show, :update]
      end
      resources :tasks, only: [:index, :show, :create, :update, :destroy]
    end
  end
end
