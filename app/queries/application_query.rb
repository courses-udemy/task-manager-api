# frozen_string_literal: true

class ApplicationQuery
  attr_accessor :relation

  PER_PAGE = 30

  delegate :first, :last, :count, :any?, to: :relation

  def all
    includes.relation
  end

  def build(attributes = {})
    all.new(attributes)
  end

  def find(id)
    relation.find_by(id: id)
  end

  def paginate(page = 1)
    includes.relation.page(page).per(PER_PAGE)
  end

  def to_select2
    relation.map do |record|
      { id: record.id, text: record.select2_text }
    end
  end

  def search_ilike_for(colums, term)
    if term
      params = { t: "%#{term.to_s.downcase}%" }
      colums = colums.map { |colum| "unaccent(#{colum}) ILIKE unaccent(:t)" }
      @relation = relation.where(colums.join(' OR '), params)
    end

    self
  end

  def by_id(id)
    @relation = relation.where(id: id) if id.present?

    self
  end

  def includes
    self
  end
end
