# frozen_string_literal: true

require 'jwt'

module JsonWebToken
  class Decode < Authentication
    def self.call(token)
      new(token).decode
    end

    def initialize(token)
      @token = token
    end

    def decode
      decoded = JWT.decode(token, hmac_secret, true, algorithm: ALGORITHM)
      JSON.parse(decoded.first.to_json, symbolize_names: true)
    rescue JWT::DecodeError
      {}
    end

    private

    attr_accessor :token
  end
end
