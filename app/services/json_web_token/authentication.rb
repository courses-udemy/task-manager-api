# frozen_string_literal: true

require 'jwt'

module JsonWebToken
  class Authentication
    private

    ALGORITHM = 'HS256'

    def expiration_token
      3.hours.from_now
    end

    def hmac_secret
      @hmac_secret ||= Rails.application.secrets.secret_key_base
    end
  end
end
