# frozen_string_literal: true

require 'jwt'

module JsonWebToken
  class Encode < Authentication
    def self.call(attributes)
      new(attributes).encode
    end

    def initialize(attributes)
      @payload = { exp: expiration_token.to_i, nbf: Time.current }.merge(attributes)
    end

    def encode
      JWT.encode(payload, hmac_secret, ALGORITHM)
    end

    private

    attr_accessor :payload
  end
end
