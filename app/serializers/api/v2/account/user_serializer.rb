# frozen_string_literal: true

module Api
  module V2
    module Account
      class UserSerializer < Api::V2::UserSerializer
      end
    end
  end
end
