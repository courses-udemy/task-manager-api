# frozen_string_literal: true

module Api
  module V2
    class UserSerializer < ApplicationSerializer
      attributes :id, :username, :email, :created_at, :updated_at
    end
  end
end
