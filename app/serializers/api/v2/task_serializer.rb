# frozen_string_literal: true

module Api
  module V2
    class TaskSerializer < ApplicationSerializer
      attributes :id, :title, :description, :completed_at, :due_date, :created_at, :updated_at
    end
  end
end
