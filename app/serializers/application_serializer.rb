# frozen_string_literal: true

class ApplicationSerializer < ActiveModel::Serializer
  delegate :r, :h, to: :object
end
