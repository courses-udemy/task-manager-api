# frozen_string_literal: true

module Account
  def self.table_name_prefix
    'account_'
  end
end
