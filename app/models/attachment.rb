# frozen_string_literal: true

module Attachment
  def self.table_name_prefix
    'attachment_'
  end
end
