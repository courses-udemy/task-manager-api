# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def self.table_name
    return super if full_table_name_prefix.present?

    prefix = model_name.name.deconstantize.underscore

    return super if prefix.blank?

    "#{prefix}_#{super}"
  end
end
