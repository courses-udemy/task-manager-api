# frozen_string_literal: true

module Attachment
  class Avatar < Archive
    has_attached_file :file, styles: {
      large: '300x300', medium: '300x300#', small: '150x150',
      tiny: '150x150#', thumb: '50x50#'
    }

    validates_attachment :file, presence: true, content_type: {
      content_type: ['image/jpg', 'image/jpeg', 'image/png', 'image/gif']
    }, size: { in: 0..1.megabytes }
  end
end
