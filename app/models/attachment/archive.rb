# frozen_string_literal: true

module Attachment
  class Archive < ApplicationRecord
    belongs_to :archivable, polymorphic: true, dependent: :destroy

    has_attached_file :file
  end
end
