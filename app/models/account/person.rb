# frozen_string_literal: true

module Account
  class Person < ApplicationRecord
    include Attachment::PersonAvatarDecorator

    belongs_to :personable, polymorphic: true, dependent: :destroy

    has_one :avatar, as: :archivable, class_name: Attachment::Avatar

    accepts_nested_attributes_for :avatar

    has_enumeration_for :gender, with: Gender, create_helpers: true
    has_enumeration_for :civil_status, with: CivilStatus, create_helpers: true

    validates :full_name, presence: true
    validates :gender, presence: true, inclusion: { in: Gender.list }
    validates :civil_status, presence: true, inclusion: { in: CivilStatus.list }
    validates :born_at, presence: true
    validates :document, presence: true, cpf: true, if: :document_required?
  end
end
