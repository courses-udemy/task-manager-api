# frozen_string_literal: true

module Account
  class User < ApplicationRecord
    devise :database_authenticatable, :registerable,
           :recoverable, :rememberable, :trackable, :validatable

    has_one :person, as: :personable

    has_many :tasks, dependent: :destroy

    accepts_nested_attributes_for :person

    validates :username, presence: true, uniqueness: true
    validates :password, length: { in: 6..128 }, on: :create
    validates :password, presence: false, length: { in: 6..128 }, on: :update, allow_blank: true

    delegate :full_name, to: :person, allow_nil: true
    delegate :document, to: :person, allow_nil: true

    before_create :generate_authentication_token!

    def generate_authentication_token!
      loop do
        self.authentication_token = Devise.friendly_token
        break unless User.exists?(authentication_token: authentication_token)
      end
    end
  end
end
