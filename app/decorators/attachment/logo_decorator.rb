# frozen_string_literal: true

module Attachment
  module LogoDecorator
    include ApplicationDecorator

    def logo_image_tag(style = nil, options = {})
      h.image_tag(logo_url(style), options.merge(alt: full_name, title: full_name))
    end

    def logo_url(style = nil)
      logo.file.url(style) if logo.present?
    end
  end
end
