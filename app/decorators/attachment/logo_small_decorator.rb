# frozen_string_literal: true

module Attachment
  module LogoSmallDecorator
    include ApplicationDecorator

    def logo_small_image_tag(style = nil, options = {})
      h.image_tag(logo_small_url(style), options.merge(alt: full_name, title: full_name))
    end

    def logo_small_url(style = nil)
      if logo_small.present?
        logo_small.file.url(style)
      else
        'missing.png'
      end
    end
  end
end
