# frozen_string_literal: true

module Attachment
  module PersonAvatarDecorator
    include ApplicationDecorator

    def avatar_image_tag(style = nil, options = {})
      h.image_tag(avatar_url(style), options.merge(alt: full_name, title: full_name))
    end

    def avatar_url(style = nil)
      if avatar.present?
        avatar.file.url(style)
      else
        default_avatar_url
      end
    end

    def default_avatar_url
      if gender.present?
        "/avatars/#{gender}.svg"
      else
        '/avatars/default.svg'
      end
    end
  end
end
