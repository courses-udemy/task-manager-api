# frozen_string_literal: true

module Attachment
  module AvatarDecorator
    include ApplicationDecorator

    delegate :avatar_image_tag, :avatar_url, :default_avatar_url, :full_name, to: :person
  end
end
