# frozen_string_literal: true

module ApplicationDecorator
  def cnpj_format(document_field = :document)
    h.cnpj_format(try(document_field))
  end

  def cpf_format(document_field = :document)
    h.cpf_format(try(document_field))
  end

  def locale_date(date_field = :created_at)
    h.locale_date(try(date_field))
  end

  def date_format(date_field, format = '%d/%m/%Y %H:%M')
    h.date_format(try(date_field), format)
  end

  def select2_text
    try(select2_column_name)
  end

  def select2_column_name
    :title
  end

  def select2_selected
    id.present? ? [select2_text, id] : []
  end

  def helpers
    @helpers ||= ApplicationController.helpers
  end
  alias h helpers

  def routes
    @routes ||= Rails.application.routes.url_helpers
  end
  alias r routes
end
