# frozen_string_literal: true

module Account
  module HasPersonDecorator
    include Attachment::AvatarDecorator

    delegate :full_name, to: :person, allow_nil: true
    delegate :document, to: :person, allow_nil: true
    delegate :avatar_image_tag, to: :person, allow_nil: true
  end
end
