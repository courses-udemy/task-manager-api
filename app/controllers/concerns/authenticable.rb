# frozen_string_literal: true

module Authenticable
  def http_authorization_header!
    authenticate_with_token!
    renovate_auth_token!
  end

  def authenticate_with_token!
    if auth_token.present?
      return if user_signed_in?
      render json: { errors: 'Unauthorized Access!' }, status: :unauthorized
    else
      render json: { errors: 'Authorization Token Missing' }, status: :forbidden
    end
  end

  def renovate_auth_token!
    return unless user_signed_in?

    payload = { user_id: current_user.id }
    response.headers['Authorization-Token'] = JsonWebToken::Encode.call(payload)
  end

  def auth_token?
    @auth_token ||= auth_token.present?
  end

  def auth_token
    @auth_token ||= request.headers['Authorization-Token']
  end

  def user_signed_in?
    @user_signed_in ||= current_user.present?
  end

  def current_user
    @current_user ||= Account::User.find_by(id: decoded_auth_token[:user_id])
  end

  def decoded_auth_token
    @decoded_auth_token ||= JsonWebToken::Decode.call(auth_token)
  end
end
