# frozen_string_literal: true

module Api
  class AuthController < Api::ApplicationController
    include Authenticable

    before_action :http_authorization_header!
  end
end
