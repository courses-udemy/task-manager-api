# frozen_string_literal: true

module Api
  module V1
    module Account
      class RegistrationsController < Api::ApplicationController
        def create
          user = ::Account::User.new(registration_params)

          if user.save
            sign_in(user, store: false)

            render json: user, status: :created
          else
            render json: { errors: user.errors }, status: :unprocessable_entity
          end
        end

        private

        def registration_params
          params.require(:registration).permit(:username, :email, :password)
        end
      end
    end
  end
end
