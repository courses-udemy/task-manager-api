# frozen_string_literal: true

module Api
  module V1
    class TasksController < Api::AuthController
      before_action :set_task, only: [:show, :update, :destroy]

      def index
        render json: { tasks: current_user.tasks }, status: :ok
      end

      def show
        render json: @task, status: :ok
      end

      def create
        @task = current_user.tasks.build(task_params)

        if @task.save
          render json: @task, status: :created
        else
          render json: { errors: @task.errors }, status: :unprocessable_entity
        end
      end

      def update
        if @task.update(task_params)
          render json: @task, status: :ok
        else
          render json: { errors: @task.errors }, status: :unprocessable_entity
        end
      end

      def destroy
        @task.destroy
      end

      private

      def set_task
        @task = current_user.tasks.find_by(id: params[:id])
        render json: { errors: I18n.t(:not_found) }, status: :not_found if @task.blank?
      end

      def task_params
        params.require(:task).permit(:title, :description, :due_date, :completed_at)
      end
    end
  end
end
