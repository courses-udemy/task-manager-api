# frozen_string_literal: true

module Api
  module V2
    module Account
      class UsersController < Api::AuthController
        def show
          render json: current_user, status: :ok
        end

        def update
          if current_user.update(user_params)
            render json: current_user, status: :ok
          else
            render json: { errors: current_user.errors }, status: :unprocessable_entity
          end
        end

        private

        def user_params
          params.require(:user).permit(:username, :email, :password, :password_confirmation)
        end
      end
    end
  end
end
