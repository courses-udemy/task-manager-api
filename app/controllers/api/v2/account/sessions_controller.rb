# frozen_string_literal: true

module Api
  module V2
    module Account
      class SessionsController < Api::ApplicationController
        def create
          user = ::Account::User.find_by(email: session_params[:email])

          if user&.valid_password?(session_params[:password])
            sign_in(user, store: false)

            response.headers['Authorization-Token'] = JsonWebToken::Encode.call(user_id: user.id)

            render json: user, status: :ok
          else
            render json: { errors: I18n.t('.login_unauthorized') }, status: :unauthorized
          end
        end

        def destroy
          user = ::Account::User.find_by(authentication_token: params[:id])
          user.update(authentication_token: nil)
        end

        private

        def session_params
          params.require(:session).permit(:email, :password)
        end
      end
    end
  end
end
