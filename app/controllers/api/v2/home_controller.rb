# frozen_string_literal: true

module Api
  module V2
    class HomeController < Api::ApplicationController
      def index
        render json: {
          app_name: ENV.fetch('APP_PAGE_TITLE')
        }
      end
    end
  end
end
