# frozen_string_literal: true

module Admin
  module Account
    class HomeController < Admin::Account::ApplicationController
      def index
      end
    end
  end
end
