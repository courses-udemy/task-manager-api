# frozen_string_literal: true

module Admin
  module Account
    class UsersController < Admin::Account::ApplicationController
      def index
        @users = ::Account::User.page(params[:page])
      end
    end
  end
end
