# frozen_string_literal: true

module Admin
  module Account
    class ApplicationController < Admin::ApplicationController
    end
  end
end
