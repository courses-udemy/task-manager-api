# frozen_string_literal: true

class ApplicationWorkers
  include Sidekiq::Worker
end
