/**
 * From Application
 */
//= require_tree ./modules

$(document).on('turbolinks:load', function() {
  APP.Propeller.init()
})
