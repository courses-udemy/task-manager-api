(function () {
  this.APP = this.APP || {};

  APP.Propeller = {
    init: function () {
      this.addClass()
      this.tooltip()
      this.tooltipTop()
      this.tooltipRight()
      this.tooltipBottom()
      this.tooltipLeft()
    },

    addClass: function () {
      $('[data-tooltip]').addClass('pmd-tooltip')
    },

    tooltip: function () {
      $('[data-tooltip]').tooltip()
    },

    tooltipTop: function () {
      $('[data-tooltip="top"]').tooltip({ placement: 'top' })
    },

    tooltipRight: function () {
      $('[data-tooltip="right"]').tooltip({ placement: 'right' })
    },

    tooltipBottom: function () {
      $('[data-tooltip="bottom"]').tooltip({ placement: 'bottom' })
    },

    tooltipLeft: function () {
      $('[data-tooltip="left"]').tooltip({ placement: 'left' })
    }
  }
}).call(this)
