# frozen_string_literal: true

class ApplicationPresenter < SimpleDelegator
  include ApplicationDecorator

  def initialize(klass)
    __setobj__(klass)
  end
end
