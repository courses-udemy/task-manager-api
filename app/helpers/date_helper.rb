# frozen_string_literal: true

module DateHelper
  def current_week
    (current_monday..current_friday)
  end

  def current_monday
    @current_monday ||= Date.current.beginning_of_week
  end

  def current_friday
    @current_friday ||= (current_monday + 4.days)
  end

  def locale_date(date)
    l(date) if date.present?
  end

  def date_format(date, format = '%d/%m/%Y %H:%M')
    date&.strftime(format) if date.present?
  end

  def random_date_in_current_month
    from = Time.current.at_beginning_of_month
    to = Time.current.at_end_of_month
    Time.zone.at(from + rand * (to.to_f - from.to_f))
  end
end
