# frozen_string_literal: true

module HtmlHelper
  def collection_to_ul_list(collection, attribute)
    result = ['<ul>']
    collection.each do |resource|
      result << "<li>#{resource.try(attribute) || '--'}</li>"
    end
    result << '</ul>'
    safe_join(result.map(&:html_safe))
  end

  def table_td(resource, attribute)
    content_tag :td, resource.try(attribute), data: { title: ta(resource.class, attribute) }
  end
end
