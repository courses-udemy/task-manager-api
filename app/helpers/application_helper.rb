# frozen_string_literal: true

module ApplicationHelper
  def app_page_title(subtitle = nil)
    subtitle = " #{subtitle} -" if subtitle.present?

    ENV.fetch('APP_PAGE_TITLE').gsub(' {{subtitle}}', subtitle.to_s)
  end

  def admin_page_title(subtitle = nil)
    "ADMIN :: #{app_page_title(subtitle)}"
  end
end
