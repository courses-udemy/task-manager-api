# frozen_string_literal: true

module PropellerMaterialDesignHelper
  def pmd_icon(icon, options = {})
    options[:class] = pmd_classes(options)
    content_tag :i, icon, options
  end

  def pmd_icon_text(icon, text, options = {})
    "#{pmd_icon(icon, options)} #{text}".html_safe
  end

  def pmd_classes(options = {})
    "material-icons #{options[:class]}".strip
  end

  def pmd_class_button_flat_ripple(additional = nil)
    "btn btn-sm pmd-btn-fab pmd-btn-flat pmd-ripple-effect #{additional}".strip
  end
end
