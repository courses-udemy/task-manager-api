# frozen_string_literal: true

module MenuHelper
  def link_to_new_modal(resource, url)
    label = label_link_to_new(resource)
    link_to label, url, data: { toggle: 'modal', target: '#modal' },
                        class: 'btn btn-primary', remote: true
  end

  def link_to_small_new_modal(resource, url)
    label = label_link_to_new(resource)
    link_to label, url, data: { toggle: 'modal', target: '#modal' },
                        class: 'btn btn-action', remote: true
  end

  def link_to_new(resource, url)
    link_to label_link_to_new(resource), url, class: 'btn btn-primary'
  end

  def label_link_to_new(resource)
    t('menu.links.new', model: tm(resource))
  end

  def link_to_show(url)
    link_to t('menu.links.show'), url, class: 'btn btn-action'
  end

  def link_to_edit_modal(url, text = nil)
    text ||= t('menu.links.edit')
    link_to text, url, class: 'btn btn-action', remote: true,
                       data: { toggle: 'modal', target: '#modal' }
  end

  def link_to_edit(url, text = nil)
    text ||= pmd_icon('edit')
    link_to text, url, title: t('menu.links.edit'),
                       data: { tooltip: 'top' },
                       class: pmd_class_button_flat_ripple('btn-info')
  end

  def link_to_destroy(url, text_confirmation = t('shared.destroy_confirmation.question'))
    modal_id = "destroy#{url.tr('/', '-')}-#{SecureRandom.uuid}"

    out = []
    out << modal_link_to_destroy(url: url, modal_id: modal_id, text_confirmation: text_confirmation)
    out << button_link_to_destroy(url, modal_id)

    safe_join(out)
  end

  def button_link_to_destroy(_url, modal_id)
    content_tag :span, pmd_icon('delete'),
                title: t('menu.links.destroy'),
                class: pmd_class_button_flat_ripple('btn-danger'),
                data: { tooltip: 'top', toggle: 'modal', target: "##{modal_id}" }
  end

  def modal_link_to_destroy(attributes)
    render 'shared/destroy_confirmation', attributes
  end
end
