# frozen_string_literal: true

module PaginationHelper
  def pagination_links(collection)
    render 'kaminari/pagination', collection: collection
  end
end
