# frozen_string_literal: true

module XEditableHelper
  def x_editable_input(resource, attribute, model: nil, url: nil)
    span = content_tag(:span, resource.try(attribute),
                       title: ta(resource.class, attribute),
                       data: { bootstrap: 'editable', pk: resource.id, name: attribute,
                               url: (url || x_editable_resource_path(resource)),
                               model: (model || x_editable_model_name(resource)) })
    content_tag(:div, span)
  end

  def x_editable_resource_path(resource)
    send "#{x_editable_model_name(resource)}_path", resource
  end

  def x_editable_model_name(resource)
    resource.class.to_s.parameterize.underscore
  end
end
