# frozen_string_literal: true

module CpfCnpjHelper
  def cpf_format(cpf)
    cpf = cpf.to_s.delete('./-')
    "#{cpf[0..2]}.#{cpf[3..5]}.#{cpf[6..8]}-#{cpf[9..11]}"
  end

  def cnpj_format(cnpj)
    cnpj = cnpj.to_s.delete('./-')
    "#{cnpj[0..1]}.#{cnpj[2..4]}.#{cnpj[5..7]}/#{cnpj[8..11]}-#{cnpj[12..14]}"
  end
end
