# frozen_string_literal: true

module GravatarHelper
  def gravatar_image_tag(email, size = 50, options = {})
    hash = Digest::MD5.hexdigest(email)
    image_tag("https://www.gravatar.com/avatar/#{hash}?size=#{size}", options)
  end
end
