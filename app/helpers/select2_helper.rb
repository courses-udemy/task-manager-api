# frozen_string_literal: true

module Select2Helper
  def select2_multiple_remote_association(f, column, url, wrapper_html: {}, input_html: {})
    select_ids = f.object.try("#{column.to_s.singularize}_ids")

    input_html = { multiple: true }.merge(input_html)
    input_html[:data] ||= {}
    input_html[:data][:select_id] ||= select_ids
    input_html[:data][:select] ||= 'remote-multiple'

    select2_remote_association(f, column, url, wrapper_html: wrapper_html, input_html: input_html)
  end

  def select2_remote_or_create_association(f, column, url, wrapper_html: {}, input_html: {})
    input_html[:data] ||= {}
    input_html[:data][:select] ||= 'remote-or-create'

    select2_remote_association(f, column, url, wrapper_html: wrapper_html, input_html: input_html)
  end

  def select2_remote_association(f, column, url, wrapper_html: {}, input_html: {})
    input_html[:data] = mount_html_data(input_html[:data], f, column)
    input_html[:data][:select_url] ||= url # || f.object.class.model_name.collection

    f.association(column, wrapper_html: wrapper_html, input_html: input_html)
  end

  def select2_remote(f, column, collection: [], wrapper_html: {}, input_html: {})
    input_html[:data] = mount_html_data(input_html[:data], f, column)

    f.input(column, collection: collection, wrapper_html: wrapper_html, input_html: input_html)
  end

  # Auxiliaries
  def mount_html_data(data, f, column)
    data = {} if data.blank?
    data[:select] ||= 'remote'
    data[:select_id] ||= f.object.try("#{column}_id")

    data
  end

  # Aliases
  def select2_remote_school_class(f, column)
    input_html = { data: { allow_clear: 'true' } }

    select2_remote_association(f, column, 'academical/school_classes', input_html: input_html)
  end
end
