# frozen_string_literal: true

module DatetimepickerHelper
  def input_datetimepicker(f, column_name, value = nil, wrapper_html: {}, input_html: {})
    input_html = { value: value || f.object.try(column_name),
                   data: { bootstrap: 'datetimepicker' } }.merge(input_html)
    f.input(column_name, as: :string, wrapper_html: wrapper_html, input_html: input_html)
  end

  def input_datetimepicker_now(f, column_name, value = nil, wrapper_html: {}, input_html: {})
    input_html = { value: value || f.object.try(column_name),
                   data: { bootstrap: 'datetimepicker-now' } }.merge(input_html)
    f.input(column_name, as: :string, wrapper_html: wrapper_html, input_html: input_html)
  end
end
