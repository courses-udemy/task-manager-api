# frozen_string_literal: true

class CivilStatus < ApplicationEnumeration
  associate_values :single, :married, :divorced
end
