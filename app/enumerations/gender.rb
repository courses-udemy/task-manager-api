# frozen_string_literal: true

class Gender < ApplicationEnumeration
  associate_values :male, :female
end
