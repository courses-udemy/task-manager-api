# frozen_string_literal: true

class Weekdays < ApplicationEnumeration
  associate_values :sunday, :monday, :tuesday, :wednesday, :thursday, :friday, :saturday
end
