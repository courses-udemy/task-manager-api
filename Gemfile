# frozen_string_literal: true

source 'https://rubygems.org'
ruby '2.4.1'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.2', '>= 5.0.2'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.20'
# Use Puma as the app server
gem 'puma', '~> 3.8'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby
# Use jquery as the JavaScript library
gem 'jquery-rails', '~> 4.3'
# Turbolinks makes navigating your web application faster.
# Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'

# https://github.com/bploetz/versionist
gem 'versionist', '~> 1.5'

# https://github.com/plataformatec/devise
gem 'devise', '~> 4.2'

# https://github.com/jwt/ruby-jwt
gem 'jwt', '~> 1.5'

# https://github.com/thoughtbot/paperclip
gem 'paperclip', '~> 5.1'

# https://github.com/lucascaton/enumerate_it
gem 'enumerate_it', '~> 1.5'

# https://github.com/rfs/validates_cpf_cnpj
gem 'validates_cpf_cnpj', '~> 0.2'

# https://github.com/plataformatec/simple_form
gem 'simple_form', '~> 3.4'

# https://github.com/rails-api/active_model_serializers
gem 'active_model_serializers', '~> 0.10.0'

# https://github.com/kaminari/kaminari
gem 'kaminari', '~> 1.0'

group :development, :test do
  # https://github.com/flyerhzm/bullet
  gem 'bullet', '~> 5.4.2'
  # https://github.com/rweng/pry-rails
  gem 'pry-rails', '~> 0.3.6'
  # https://github.com/rspec/rspec-rails
  gem 'rspec-rails', '~> 3.5', '>= 3.5.1'
  # https://github.com/bbatsov/rubocop
  gem 'rubocop', '~> 0.48'
  # https://github.com/backus/rubocop-rspec
  gem 'rubocop-rspec', '~> 1.15'
  # https://github.com/thoughtbot/factory_girl_rails
  gem 'factory_girl_rails', '~> 4.8'
  # https://github.com/stympy/faker
  gem 'faker', '~> 1.7'
  # https://github.com/bernardo/cpf_faker
  gem 'cpf_faker', '~> 1.3'
  # https://github.com/bkeepers/dotenv
  gem 'dotenv-rails', '~> 2.2'
  # https://github.com/presidentbeef/brakeman
  gem 'brakeman', '~> 3.6', require: false
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background.
  # Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  # https://github.com/jonleighton/spring-commands-rspec
  gem 'spring-commands-rspec', require: false
end

group :test do
  # https://github.com/DatabaseCleaner/database_cleaner
  gem 'database_cleaner', '~> 1.5'
  # https://github.com/thoughtbot/shoulda-matchers
  gem 'shoulda-matchers', '~> 3.1'
  # https://github.com/rails/rails-controller-testing
  gem 'rails-controller-testing', '~> 0.1.1'
  # https://github.com/colszowka/simplecov
  gem 'simplecov', require: false
end
